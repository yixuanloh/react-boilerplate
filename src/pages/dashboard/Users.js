import React from "react";
import { useGetUsersQuery } from "../../features/user/userApiSlice";

const Users = () => {
  const {
    data: users,
    isLoading,
    isSuccess,
    isError,
    error,
  } = useGetUsersQuery();

  return (
    <>
      {!isLoading && isSuccess ? (
        <>
          <h1>Users List</h1>
          <ul>
            {users.map((user, i) => {
              return <li key={i}>{user.username}</li>;
            })}
          </ul>
        </>
      ) : (
        <p>Nothing</p>
      )}
    </>
  );
};

export default Users;

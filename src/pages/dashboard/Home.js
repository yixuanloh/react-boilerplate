import React from "react";
import { logout } from "../../features/auth/authSlice";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";

const Home = () => {
  const dispatch = useDispatch();

  return (
    <>
      <h1 className="text-3xl font-bold underline">Hello world!</h1>
      <Link to="/users">Users</Link>
      <button
        type="submit"
        className="button"
        onClick={() => dispatch(logout())}
      >
        Logout
      </button>
    </>
  );
};

export default Home;

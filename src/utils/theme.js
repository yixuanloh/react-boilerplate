import { ThemeContext } from "@emotion/react";
import { createTheme, colors } from "@mui/material";

const theme = createTheme({
  palette: {
    primary: {
      main: colors.grey[500],
    },
  },
});

export default theme;
